# Carrying information from @expectedException, @expectedExceptionMessage to expectedException() and expectedExceptionMessage().


# for Notepad++

1st step: Carrying information from @expectedExceptionMessage to expectedExceptionMessage().

Find what:
```
  @expectedExceptionMessage ([^\n]+?)\n     \*([\w\W]+?\{)
```

Replace with:
```
$2\n        $this->expectedExceptionMessage\("$1"\);
```

2nd step: Carrying information from @expectedException to expectedException().

Find what:
```
  @expectedException ([^\n]+?)\n     \*([\w\W]+?\{)
```

Replace with:
```
$2\n        $this->expectException\("$1"\);
```

3rd step: Removes empty comments.

Find what:
```
    /**
     */
```

Replace with:
<nothing>

